#!/bin/bash
set -e


# instalar apache2 y git
apt update && apt install apache2 git -y


if test -d /app;then
	
	# esto se ejecuta siempre que la condicion devuelva true.
	cd /app && git pull
else
	# esto se ejecuta siempre que la condicion devuelva false.
	git clone https://gitlab.com/sergio.pernas1/dashboard.git /app
fi



cd /app && git checkout $BRANCHAPP

cp /app/apache2.conf /etc/apache2/

cp /app/000-default.conf /etc/apache2/sites-available/



exec "$@" 
